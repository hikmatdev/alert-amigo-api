const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer");
const checkAuth = require("../middleware/check-auth");
const _ = require("lodash");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    console.log(req.file);
    cb(
      null,
      new Date().toISOString().replace(/:|\./g, "-") + file.originalname
    );
  },
});

const fileFilter = (req, file, cb) => {
  //reject a file
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(
      new Error(
        "message: cannot upload other formats please, jpeg, png file format only"
      ),
      false
    );
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
});

const Product = require("../models/product");

//---------------GET REQUEST----------------
/* router.get("/", (req, res, next) => {
  Product.find()
    .select(
      "_id productName productPrice productCategory productBrand countryOfOrigin riskType alertSubmittedBy yourCity yourAddress  productImage description"
    )
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        products: docs.map(doc => {
          return {
            _id: doc._id,
            productName: doc.productName,
            productPrice: doc.productPrice,
            productCategory: doc.productCategory,
            productBrand: doc.productBrand,
            countryOfOrigin: doc.countryOfOrigin,
            riskType: doc.riskType,
            alertSubmittedBy: doc.alertSubmittedBy,
            yourCity: doc.yourCity,
            yourAddress: doc.yourAddress,
            productImage: doc.productImage,
            description: doc.description,
            request: {
              type: "GET",
              url: "https://alert-amigo-api.herokuapp.com/products/" + doc._id
            }
          };
        })
      };
      console.log(docs);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
}); */

//---------------POST REQUEST----------------
router.post("/", upload.single("productImage"), (req, res, next) => {
  //if (!req.file || !req.file.path) return res.send('Please upload a file')

  console.log(req.body);
  console.log(req.file);
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    productName: req.body.productName,
    productPrice: req.body.productPrice,
    productCategory: req.body.productCategory,
    productBrand: req.body.productBrand,
    countryOfOrigin: req.body.countryOfOrigin,
    riskType: req.body.riskType,
    alertSubmittedBy: req.body.alertSubmittedBy,
    yourCity: req.body.yourCity,
    yourAddress: req.body.yourAddress,
    productImage: req.file.path,
    description: req.body.description,
  });
  product
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).json({
        message: "Created a product successfully!",
        createdProduct: {
          _id: result._id,
          productName: result.productName,
          productPrice: result.productPrice,
          productCategory: result.productCategory,
          productBrand: result.productBrand,
          countryOfOrigin: result.countryOfOrigin,
          riskType: result.riskType,
          alertSubmittedBy: result.alertSubmittedBy,
          yourCity: result.yourCity,
          yourAddress: result.yourAddress,
          productImage: result.productImage,
          description: result.description,

          request: {
            type: "POST",
            url: "https://alert-amigo-api.herokuapp.com/products/" + result._id,
          },
        },
      });
    })
    .catch((err) => {
      console.log(err.response);
      res.status(500).json({
        error: err,
      });
    });
});

//---------------GET REQUEST WITH ID----------------
router.get("/:productId", (req, res, next) => {
  const id = req.params.productId;
  Product.findById(id)
    .select(
      "_id productName productPrice productCategory productBrand countryOfOrigin riskType alertSubmittedBy yourCity yourAddress  productImage description"
    )
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          products: doc,
          request: {
            type: "GET",
            url: "http://localhost:3000/products",
          },
        });
      } else {
        res.status(404).json({
          message: "the id couldnot be found!!!",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//---------------GET REQUEST WITH Product Name----------------
router.get("/", (req, res, next) => {
  var response = [];
  console.log(req.query);

  Product.find(req.query)
    .select(
      "_id productName productPrice productCategory productBrand countryOfOrigin riskType alertSubmittedBy yourCity yourAddress  productImage description"
    )
    .exec()
    .then((docs) => {
      const response = {
        count: docs.length,
        products: docs.map((doc) => {
          return {
            _id: doc._id,
            productName: doc.productName,
            productPrice: doc.productPrice,
            productCategory: doc.productCategory,
            productBrand: doc.productBrand,
            countryOfOrigin: doc.countryOfOrigin,
            riskType: doc.riskType,
            alertSubmittedBy: doc.alertSubmittedBy,
            yourCity: doc.yourCity,
            yourAddress: doc.yourAddress,
            productImage: doc.productImage,
            description: doc.description,
            request: {
              type: "GET",
              url: "https://alert-amigo-api.herokuapp.com/products/" + doc._id,
            },
          };
        }),
      };
      console.log(docs);
      res.status(200).json(response);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

//---------------PATCH REQUEST WITH ID----------------
router.patch("/:productId", (req, res, next) => {
  const id = req.params.productId;
  const updateOps = {};
  for (const ops of Object.keys(updateOps)) {
    updateOps[ops.proName] = ops.value;
  }
  Product.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json({
        message: "product updated!",
        request: {
          type: "GET",
          url: "https://alert-amigo-api.herokuapp.com/products/" + id,
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//---------------DELETE REQUEST WITH ID----------------
router.delete("/:productId", (req, res, next) => {
  const id = req.params.productId;
  Product.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "product deleted",
        request: {
          type: "POST",
          url: "https://alert-amigo-api.herokuapp.com/products/",
          body: {
            name: "String",
            price: "Number",
          },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
