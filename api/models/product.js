const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  productName: { type: String },
  productPrice: { type: Number },
  productCategory: { type: String },
  productBrand: { type: String },
  countryOfOrigin: { type: String },
  riskType: { type: String },
  alertSubmittedBy: { type: String },
  yourCity: { type: String },
  yourAddress: { type: String },
  productImage: { type: String },
  description: { type: String },
});

module.exports = mongoose.model("Product", productSchema);
